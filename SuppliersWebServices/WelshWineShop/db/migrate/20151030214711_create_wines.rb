class CreateWines < ActiveRecord::Migration
  def change
    create_table :wines do |t|
      t.string :name, null: false
      t.string :image_url
      t.string :short_description, null: false
      t.float :bottle_size_in_ml, null: false
      t.float :price, null: false
      t.string :long_description
      t.string :country_of_origin
      t.string :grade_type
      t.boolean :suitable_for_vegetarians, null: false

      t.timestamps
    end
  end
end
