# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151120233611) do

  create_table "orders", force: true do |t|
    t.string   "customer_name"
    t.string   "customer_address"
    t.string   "customer_email"
    t.integer  "quantity"
    t.integer  "wine_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "orders", ["wine_id"], name: "index_orders_on_wine_id"

  create_table "wines", force: true do |t|
    t.string   "name",                     null: false
    t.string   "image_url"
    t.string   "short_description",        null: false
    t.float    "bottle_size_in_ml",        null: false
    t.float    "price",                    null: false
    t.string   "long_description"
    t.string   "country_of_origin"
    t.string   "grade_type"
    t.boolean  "suitable_for_vegetarians", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

end
