# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Wine.transaction do
    1...30.times do |i|
        wine = Wine.create!(name: "Wine#{i}",
                           image_url: "/system/wines/images/000/000/002/original/wine_PNG9479.png",
                           short_description: "WineShortDescription#{i}",
                           bottle_size_in_ml: Random.rand(1..200),
                           price: Random.rand(1..50),
                           long_description: "WineLongDescription#{i}",
                           country_of_origin: "WineCountryOfOrigin#{i}",
                           grade_type: "WineGradeType#{i}",
                           suitable_for_vegetarians: true,
                           image: File.new("#{Rails.root}/public/system/wines/images/000/000/011/original/wine_PNG9479.png")
                           )
    end
end