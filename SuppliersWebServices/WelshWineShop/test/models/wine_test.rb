require 'test_helper'

class WineTest < ActiveSupport::TestCase
  fixtures :wines
  # test "the truth" do
  #   assert true
  # end

  test "wine attributes must not be empty" do
    wine = Wine.new
    assert wine.invalid?
    assert wine.errors[:name].any?
    assert wine.errors[:short_description].any?
    assert wine.errors[:price].any?
    assert wine.errors[:bottle_size_in_ml].any?
  end

  test "wine price must be positive" do
    wine = Wine.new(name: "TestWine",
            short_description: "test short description",
            image_url: "testImageUrl.jpg",
            bottle_size_in_ml: 12,
            price: 32,
            suitable_for_vegetarians: true)
    wine.price = -1
    assert wine.invalid?
    assert_equal "must be greater than or equal to 0.01", wine.errors[:price].join('; ')

    wine.price = 0
    assert wine.invalid?
    assert_equal "must be greater than or equal to 0.01", wine.errors[:price].join('; ')    

    wine.price = 1
    assert wine.valid?
  end

  test "wine is not valid without a unique name" do
    wine = Wine.new(name: wines(:valid_wine).name,
            short_description: "test short description",
            image_url: "testImageUrl.jpg",
            bottle_size_in_ml: 12,
            price: 21,
            suitable_for_vegetarians: true)

    assert !wine.save
    assert_equal "has already been taken", wine.errors[:name].join('; ')  
  end
end
