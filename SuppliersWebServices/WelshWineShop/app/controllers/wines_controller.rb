class WinesController < ApplicationController
  before_action :set_wine, only: [:show, :edit, :update, :destroy]

  # GET /wines
  # GET /wines.json
  def index
    @wines = Wine.all
    @wines = set_wine_image @wines

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render xml: @wines }
      format.json { render json: @wines }
    end
  end

  # Method gets wines image absolute URL and stores it inside another array
  def set_wine_image(wines)
    @all_wine = Array.new
    wines.each do |wine|
      wine.image_url = wine.image.url
      @all_wine.push(wine)
    end
    @all_wine
  end

  # GET /wines/1
  # GET /wines/1.json
  def show
    respond_to do |format|
      if stale?(etag: @wine, last_modified: @wine.updated_at)
        format.html # show.html.erb
        format.xml  { render xml: @wine }
        format.json { render json: @wine }
      end
    end
  end

  # GET /wines/new
  def new
    @wine = Wine.new
  end

  # GET /wines/1/edit
  def edit
  end

  # POST /wines
  # POST /wines.json
  def create
    @wine = Wine.new(wine_params)

    # Set wine's image absolute URL as another column in the DB table 
    # - such column is exposed to web clients
    @wine.image_url = @wine.image.url

    respond_to do |format|
      if @wine.save
        format.html { redirect_to @wine, notice: 'Wine was successfully created.' }
        format.json { render :show, status: :created, location: @wine }
      else
        format.html { render :new }
        format.json { render json: @wine.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /wines/1
  # PATCH/PUT /wines/1.json
  def update
    respond_to do |format|
      if @wine.update(wine_params)
        format.html { redirect_to @wine, notice: 'Wine was successfully updated.' }
        format.json { render :show, status: :ok, location: @wine }
      else
        format.html { render :edit }
        format.json { render json: @wine.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wines/1
  # DELETE /wines/1.json
  def destroy
    @wine.destroy
    respond_to do |format|
      format.html { redirect_to wines_url, notice: 'Wine was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wine
      @wine = Wine.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wine_params
      params.require(:wine).permit(:name, :data, :image_url, :short_description, :image, :bottle_size_in_ml, :price, :long_description, :country_of_origin, :grade_type, :suitable_for_vegetarians)
    end
end
