json.array!(@orders) do |order|
  json.extract! order, :id, :customer_name, :customer_address, :customer_email, :quantity, :wine_id
  json.url order_url(order, format: :json)
end
