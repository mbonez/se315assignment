json.array!(@wines) do |wine|
  json.extract! wine, :id, :name, :image_url, :short_description, :bottle_size_in_ml, :price, :long_description, :country_of_origin, :grade_type, :image, :suitable_for_vegetarians
  json.url wine_url(wine, format: :json)
end
