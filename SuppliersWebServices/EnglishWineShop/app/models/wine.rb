class Wine < ActiveRecord::Base
	validates :name, :short_description, :bottle_size_in_ml, :price, presence: true
	validates :price, numericality: {greater_than_or_equal_to: 0.01}
	validates :name, uniqueness: true
	has_many :orders, dependent: :destroy

	has_attached_file :image
  	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  	#Get the most recently added wine
  	def self.latest
  		Wine.order('updated_at desc').limit(1).first
  	end
end
