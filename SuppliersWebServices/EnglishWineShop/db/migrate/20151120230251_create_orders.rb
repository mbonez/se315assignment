class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :customer_name
      t.string :customer_address
      t.string :customer_email
      t.integer :quantity
      t.references :wine, index: true

      t.timestamps
    end
  end
end
