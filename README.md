# README for Ruby on Rails RESTful Wine Comparison Website#

This was a major university assignment - worth 50% of a 20 credit final year module. 

Got 96% for the assignment.

It is a prototype of a wine comparison website that is RESTful, which acquire wine information from two RESTful wine suppliers and shows the cheapest wines. A customer can also order wines.

Uses technologies including:

* Language & MVC Framework - Ruby On Rails
* Interoperability - REST
* Asynchronous Tasks (e.g. Search) - AJAX
* Behaviour-Driven Tests - Cucumber Tool
* Checking Suppliers Status - HTTP Etags
* Database - SQLite (Dev) & PostGreSQL (Prod)
* Deployed to Cloud PaaS - Heroku
* Cloud Data Storage (Images) - Amazon S3


The URL to the wine comparison site on heroku:

* https://maf-heroku-ola.herokuapp.com/

URL to RESTful supplier:

* http://englishsupplier-heroku-ola.herokuapp.com 
* http://welshsupplier-heroku-ola.herokuapp.com/



![Capture.JPG](https://bitbucket.org/repo/LLqp44/images/3509566002-Capture.JPG)