class AddLongDescriptionAndCountryOfOriginAndGradeTypeAndSuitableForVegetariansAndImageFileNameToWines < ActiveRecord::Migration
  def change
    add_column :wines, :long_description, :string
    add_column :wines, :country_of_origin, :string
    add_column :wines, :grade_type, :string
    add_column :wines, :suitable_for_vegetarians, :boolean
    add_column :wines, :image_file_name, :string
  end
end
