class AddShortDescriptionAndBottleSizeInMlAndPriceToWines < ActiveRecord::Migration
	def change
		add_column :wines, :short_description, :string
		add_column :wines, :bottle_size_in_ml, :string
		add_column :wines, :price, :float

		change_column :wines, :bottle_size_in_ml, :float
	end
end
