class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :first_name, null: false
      t.string :surname, null: false
      t.string :address
      t.string :email, null: false

      t.timestamps
    end

    add_index:customers, :surname
    add_index:customers, :email
  end
end
