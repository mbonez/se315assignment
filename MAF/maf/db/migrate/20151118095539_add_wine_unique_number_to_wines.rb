class AddWineUniqueNumberToWines < ActiveRecord::Migration
  def change
    add_column :wines, :wine_unique_number, :integer
  end
end
