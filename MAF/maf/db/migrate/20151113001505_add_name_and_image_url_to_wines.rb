class AddNameAndImageUrlToWines < ActiveRecord::Migration
  def change
    add_column :wines, :name, :string
    add_column :wines, :image_url, :string
  end
end
