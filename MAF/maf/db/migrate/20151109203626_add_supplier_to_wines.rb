class AddSupplierToWines < ActiveRecord::Migration
  def change
    add_column :wines, :supplier, :string
  end
end
