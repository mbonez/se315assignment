class AddLastModifiedToWines < ActiveRecord::Migration
  def change
    add_column :wines, :last_modified, :datetime
  end
end
