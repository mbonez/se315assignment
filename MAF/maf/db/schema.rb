# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151118095539) do

  create_table "carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers", force: true do |t|
    t.string   "first_name",      null: false
    t.string   "surname",         null: false
    t.string   "address"
    t.string   "email",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
  end

  add_index "customers", ["email"], name: "index_customers_on_email"
  add_index "customers", ["surname"], name: "index_customers_on_surname"

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority"

  create_table "line_items", force: true do |t|
    t.integer  "wine_id"
    t.integer  "cart_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity",   default: 1
  end

  create_table "wines", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "supplier"
    t.string   "name"
    t.string   "image_url"
    t.string   "short_description"
    t.float    "bottle_size_in_ml",        limit: 255
    t.float    "price"
    t.string   "long_description"
    t.string   "country_of_origin"
    t.string   "grade_type"
    t.boolean  "suitable_for_vegetarians"
    t.string   "image_file_name"
    t.datetime "last_modified"
    t.integer  "wine_unique_number"
  end

end
