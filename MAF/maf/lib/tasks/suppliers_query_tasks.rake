require "#{Rails.root}/app/helpers/application_helper"
require "#{Rails.root}/app/controllers/application_controller"
require "#{Rails.root}/app/controllers/sessions_controller"
require 'rexml/document'
require 'open-uri'
include ApplicationHelper

desc 'get wines from suppliers'
task get_wines_from_suppliers: :environment do
	get_all_wines(true)
end