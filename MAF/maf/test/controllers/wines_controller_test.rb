require 'test_helper'

class WinesControllerTest < ActionController::TestCase
  include WinesHelper

  setup do
    @wine = wines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should show wine" do
    get :show, id: @wine
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wine
    assert_response :success
  end

  test "should destroy wine" do
    assert_difference('Wine.count', -1) do
      delete :destroy, id: @wine
    end

    assert_redirected_to wines_url(page: 1)
  end

  test "get wine details from web services" do
    suppliers_to_process = Array.new
    supplier_name = SUPPLIER_ONE_NAME
    suppliers_to_process << supplier_name

    wines = get_wine_details("http://localhost:3001/wines.xml", supplier_name, suppliers_to_process)
    assert_not_empty(wines, "Wines shouldn't be empty") 
  end
end
