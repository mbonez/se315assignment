require 'test_helper'

class CustomersControllerTest < ActionController::TestCase
  setup do
    @input_attributes = {
      email: "sam@adeyemi.com",
      first_name: "olu",
      surname: "ade",
      password: "private",
      password_confirmation: "private"
    }

    @customer = customers(:one)
  end

  test "should get index" do
    get :index
    assert_response :redirect
    assert_not_nil assigns(:customers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post :create, customer: @input_attributes
    end

    assert_redirected_to wines_path
  end

  test "should show customer" do
    get :show, id: @customer
    assert_response :redirect
  end

  test "should get edit" do
    get :edit, id: @customer
    assert_response :success
  end

  test "should update customer" do
    patch :update, id: @customer, customer: @input_attributes
    assert_redirected_to wines_path
  end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete :destroy, id: @customer
    end

    assert_redirected_to customers_path
  end
end
