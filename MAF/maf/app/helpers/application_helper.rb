module ApplicationHelper
  DEV_SUPPLIER_ONE_ROOT_URL = "http://localhost:3001"
  DEV_SUPPLIER_TWO_ROOT_URL = "http://localhost:3002"
  PROD_SUPPLIER_ONE_ROOT_URL = "https://englishsupplier-heroku-ola.herokuapp.com"
  PROD_SUPPLIER_TWO_ROOT_URL = "https://welshsupplier-heroku-ola.herokuapp.com" 

  SUPPLIER_ONE_NAME = "English Wine Shop"
  DEV_SUPPLIER_ONE_URL = "#{DEV_SUPPLIER_ONE_ROOT_URL}/wines.xml"
  PROD_SUPPLIER_ONE_URL = "#{PROD_SUPPLIER_ONE_ROOT_URL}/wines.xml"
  
  SUPPLIER_TWO_NAME = "Welsh Wine Shop"
  DEV_SUPPLIER_TWO_URL = "#{DEV_SUPPLIER_TWO_ROOT_URL}/wines.xml"
  PROD_SUPPLIER_TWO_URL = "#{PROD_SUPPLIER_TWO_ROOT_URL}/wines.xml"
  
  #Orders
  DEV_SUPPLIER_ONE_ORDER_URL = "#{DEV_SUPPLIER_ONE_ROOT_URL}/orders"
  DEV_SUPPLIER_TWO_ORDER_URL = "#{DEV_SUPPLIER_TWO_ROOT_URL}/orders"
  PROD_SUPPLIER_ONE_ORDER_URL = "#{PROD_SUPPLIER_ONE_ROOT_URL}/orders"
  PROD_SUPPLIER_TWO_ORDER_URL = "#{PROD_SUPPLIER_TWO_ROOT_URL}/orders"

  # Method calls other methods that gets details about wines from web services
  # and process the data - returning lists of best priced wines from 2 suppliers  
  def get_all_wines(is_rake_job)
    @supplier_wines = get_wine_details(is_rake_job)

    if !@supplier_wines[SUPPLIER_ONE_NAME].nil? || !@supplier_wines[SUPPLIER_TWO_NAME].nil?
      process_wine_data(@supplier_wines[SUPPLIER_ONE_NAME], @supplier_wines[SUPPLIER_TWO_NAME])
    end
  end

  # Method checks if data from a web service (supplier) has been updated or not using etag
  def get_supplier_data_state(supplier_url, supplier_name)
    open(supplier_url) do |feed|
      @etag = feed.meta['etag']

      # Compare new etag with the one stored in session
      if session[supplier_name] && session[supplier_name] != @etag
        return @etag
      else
        session[supplier_name] = @etag
      end
    end
  end

  def get_environment_supplier_url
    @supplier_url_one = DEV_SUPPLIER_ONE_URL
    @supplier_url_two = DEV_SUPPLIER_TWO_URL
    
    #Check if Dev of Prod
    if Rails.env.production?
      @supplier_url_one = PROD_SUPPLIER_ONE_URL
      @supplier_url_two = PROD_SUPPLIER_TWO_URL
    end
    return @supplier_url_one, @supplier_url_two
  end

  # Method checks if data from a web service (supplier) has been updated or not using etag
  def get_suppliers_data_state(is_rake_job)
    @read_from_suppliers = false

    if is_rake_job == true
    	@read_from_suppliers = true
    	return @read_from_suppliers
    end

    #Check if Dev of Prod
    @supplier_url_one, @supplier_url_two = get_environment_supplier_url

    open(@supplier_url_one) do |feed|
      @etag = feed.meta['etag']
      # Compare new etag with the one stored in session
      if !session[SUPPLIER_ONE_NAME].nil?
        if session[SUPPLIER_ONE_NAME] != @etag
          @read_from_suppliers = true
          session[SUPPLIER_ONE_NAME] = @etag
        end
      else
        session[SUPPLIER_ONE_NAME] = @etag
        @read_from_suppliers = true
      end
    end

    open(@supplier_url_two) do |feed|
      @etag = feed.meta['etag']
      # Compare new etag with the one stored in session
      if !session[SUPPLIER_TWO_NAME].nil?
        if session[SUPPLIER_TWO_NAME] != @etag
          @read_from_suppliers = true
          session[SUPPLIER_TWO_NAME] = @etag
        end
      else
        session[SUPPLIER_TWO_NAME] = @etag
        @read_from_suppliers = true
      end
    end

    @read_from_suppliers
  end  
  

  # Method gets details of wines from a web service depending on if the data has been updated or not
  # It doesn't get any data if the web service's data hasn't changed.
  def get_wine_details(is_rake_job)
    supplier_wines = {}

    # Parse the XML document into a data structure.
    @should_read = get_suppliers_data_state(is_rake_job)
    @supplier_url_one, @supplier_url_two = get_environment_supplier_url
    suppliers = { SUPPLIER_ONE_NAME => @supplier_url_one, SUPPLIER_TWO_NAME => @supplier_url_two }

    if @should_read == true      
      Cart.delete_all
      LineItem.delete_all
      
      suppliers.each_key do |supplier_name| 
        supplier_url = suppliers[supplier_name]
        supplier_xml = open(supplier_url).read    
        document = REXML::Document.new(supplier_xml)
        @the_wines = Array.new

        # Get wine data and store in an array
        document.elements.each("wines/wine") do |wine|      
          w = Wine.new    
          w.wine_unique_number = wine.elements["id"].text   
          w.last_modified = wine.elements["updated-at"].text      
          w.name = wine.elements["name"].text
          w.short_description = wine.elements["short-description"].text
          w.image_url = wine.elements["image-url"].text
          w.bottle_size_in_ml = wine.elements["bottle-size-in-ml"].text
          w.price = wine.elements["price"].text
          w.long_description = wine.elements["long-description"].text
          w.country_of_origin = wine.elements["country-of-origin"].text
          w.grade_type = wine.elements["grade-type"].text
          w.image_file_name = wine.elements["image-file-name"].text
          w.suitable_for_vegetarians = wine.elements["suitable-for-vegetarians"].text      
          w.supplier = supplier_name
          @the_wines.push(w)
        end

        supplier_wines[supplier_name] = @the_wines
      end 

      return supplier_wines
    else
      supplier_wines = Hash.new
      return supplier_wines
    end
  end


  # Get wine's name from array
  def get_wine_names_from_array(wines)
	  @wine_names = Array.new
	  wines.each do |wine|
	    @wine_names.push(wine.name)
	  end
	  @wine_names
  end


  # Method gets wine data from two suppliers and process them to return a 
  # list with the best priced wines
  def process_wine_data(supplier_one_wines, supplier_two_wines)
    @final_wines = Array.new
    @common_wines_in_supplier_one = Array.new
    @common_wines_in_supplier_two = Array.new

    if supplier_one_wines.any? || supplier_two_wines.any?
      @supplier_one_wine_names = get_wine_names_from_array supplier_one_wines
      @supplier_two_wine_names = get_wine_names_from_array supplier_two_wines
    end
  
    #First add wines that are unique to one supplier and separate the common wines
    supplier_one_wines.each do |wine|
      if @supplier_two_wine_names.include?(wine.name)
        @common_wines_in_supplier_one.push(wine)
      else
        @final_wines.push(wine)
      end
    end

    #The add the remaining wines that are unique to the other supplier
    supplier_two_wines.each do |wine|
      if @supplier_one_wine_names.include?(wine.name) 
        @common_wines_in_supplier_two.push(wine)        
      else
        @final_wines.push(wine)
      end
    end

    #Finally, deal with Common wines - add wine with lowest price
    @common_wines_in_supplier_one.each do |wine|
      w = @common_wines_in_supplier_two.find{ |a_wine|
        if a_wine.name == wine.name
          if a_wine.price >= wine.price
            @final_wines.push(wine)
          elsif a_wine.price < wine.price
            @final_wines.push(a_wine)
          end
        end
      }
    end
    
    # sort the final array by wine name without case sensitivity and save wines into DB
    @final_wines.sort! {|a,b| a.name.downcase <=> b.name.downcase}
    if @final_wines.any?
      save_processed_wines_into_db @final_wines
    end
  end


  def save_processed_wines_into_db(wines)
    #Delete everything first if DB is out of date
    Wine.destroy_all
    Wine.delete_all

    @temp_wines = Wine.all 

    # Save processed wine data into the DB
    wines.each do |wine|
      w = Wine.new
      w.wine_unique_number = wine.wine_unique_number
      w.last_modified = wine.last_modified      
      w.name = wine.name
      w.short_description = wine.short_description
      w.image_url = wine.image_url
      w.bottle_size_in_ml = wine.bottle_size_in_ml
      w.price = wine.price
      w.long_description = wine.long_description
      w.country_of_origin = wine.country_of_origin
      w.grade_type = wine.grade_type
      w.image_file_name = wine.image_file_name
      w.suitable_for_vegetarians = wine.suitable_for_vegetarians
      w.supplier = wine.supplier
            
      w.save!
    end
  end
end
