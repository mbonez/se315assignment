module CartsHelper
	include ApplicationHelper


	# Gets order details and rebuild attributes into json format
	# ready to be sent to web service
	def build_order_detail_json(item)
		@customer = Customer.find(session[:customer_id])

		json_order = {
		  :customer_name => "#{@customer.first_name} #{@customer.surname}",
		  :customer_address => @customer.address,
		  :customer_email => @customer.email,
		  :wine_id => item.wine.wine_unique_number,
		  :quantity => item.quantity
		}.to_json
	end 

	
	# Checks if an item in the cart belongs to a certain supplier
	def post_to_supplier(cart, supplier)
		@make_post = false
		cart.line_items.each do |item|
		  if item.wine.supplier.include? supplier
		    @make_post = true
		  end 
		end
		@make_post 
	end


end
