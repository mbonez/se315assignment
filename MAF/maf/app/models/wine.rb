class Wine < ActiveRecord::Base
  has_many :line_items

  before_destroy :ensure_not_referenced_by_any_line_item

  # Set wine name
  def name=(value)
    write_attribute :name, (value ? value.humanize : nil)
  end

  # Method does a SQL-style 'select where' to get anything in Wines table
  # that has anything from the 'query' parameter
  def self.like(query)
    if query.nil?
      []
    else
      if Rails.env.development?
          where('name LIKE :query ' +
            'OR short_description LIKE :query ' +
            'OR supplier LIKE :query ' +
            'OR price LIKE :query ' +
            'OR bottle_size_in_ml LIKE :query ' +
            'OR country_of_origin LIKE :query ' +
            'OR grade_type LIKE :query ' +
            'OR long_description LIKE :query',
            query: "%#{query}%")
      elsif Rails.env.production?
        where('name LIKE :query ' +
            'OR short_description LIKE :query ' +
            'OR supplier LIKE :query ' +
            'OR text(price) LIKE :query ' +
            'OR text(bottle_size_in_ml) LIKE :query ' +
            'OR country_of_origin LIKE :query ' +
            'OR grade_type LIKE :query ' +
            'OR long_description LIKE :query',
            query: "%#{query}%")
      end
    end
  end

  def self.price_like(lowest_price_query, highest_price_query)
    if lowest_price_query.nil? && highest_price_query.nil?
      []
    else
      where("price >= :lowest_price_query AND price <= :highest_price_query",
        {lowest_price_query: lowest_price_query, 
        highest_price_query: highest_price_query})
    end
  end

  # Setting number of wines on a page - used by pagination gem
  self.per_page = 4

  private
  # ensure that there are no line items referencing this product
    def ensure_not_referenced_by_any_line_item
      if line_items.empty?
        return true
      else
        errors.add(:base, 'Line Items present')
        return false
      end
    end
end