class LineItem < ActiveRecord::Base
	belongs_to :wine
	belongs_to :cart

	# Calculate total price of one item in a cart
	def total_price
		if !wine.nil?
			wine.price * quantity
		else
			0
		end
	end
end
