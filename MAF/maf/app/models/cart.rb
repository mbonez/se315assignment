class Cart < ActiveRecord::Base
	has_many :line_items, dependent: :destroy


	# Adds wine & quantity of wines being purchased to Lineitem
	# Creates new LineItem if one doesn't exist already
	def add_wine(wine_id, quantity)
		current_item = line_items.find_by_wine_id(wine_id)
		if current_item
			if quantity != nil && quantity > 1
				current_item.quantity += quantity
			else				
				current_item.quantity += 1
			end
		else
			if quantity != nil
				current_item = line_items.build(wine_id: wine_id, quantity: quantity)
			else
				current_item = line_items.build(wine_id: wine_id)
			end
		end
		
		current_item
	end

	# Calculate total price of items in a cart
	def total_price
		line_items.to_a.sum { |item| item.total_price }
	end

	# Calculate total quantity of items in a cart
	def total_quantity
		line_items.to_a.sum { |item| item.quantity }
	end

end
