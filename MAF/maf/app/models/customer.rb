class Customer < ActiveRecord::Base
	validates :email, presence: true, uniqueness: true
	has_secure_password
	validates :password, length: { minimum: 1 }, allow_nil: true
	validates_format_of :email,
                      with: /\A([\w\.\-\+]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                      message: 'Bad email address format'                      
end
