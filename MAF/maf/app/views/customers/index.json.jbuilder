json.array!(@customers) do |customer|
  json.extract! customer, :id, :first_name, :surname, :address, :email
  json.url customer_url(customer, format: :json)
end
