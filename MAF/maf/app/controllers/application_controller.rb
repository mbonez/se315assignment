class ApplicationController < ActionController::Base
  helper_method :get_customer, :get_session_id, :has_cart?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  DEV_SUPPLIER_ONE_ROOT_URL = "http://localhost:3001"
  DEV_SUPPLIER_TWO_ROOT_URL = "http://localhost:3002"
  PROD_SUPPLIER_ONE_ROOT_URL = "https://englishsupplier-heroku-ola.herokuapp.com"
  PROD_SUPPLIER_TWO_ROOT_URL = "https://welshsupplier-heroku-ola.herokuapp.com" 

  SUPPLIER_ONE_NAME = "English Wine Shop"
  DEV_SUPPLIER_ONE_URL = "#{DEV_SUPPLIER_ONE_ROOT_URL}/wines.xml"
  PROD_SUPPLIER_ONE_URL = "#{PROD_SUPPLIER_ONE_ROOT_URL}/wines.xml"
  
  SUPPLIER_TWO_NAME = "Welsh Wine Shop"
  DEV_SUPPLIER_TWO_URL = "#{DEV_SUPPLIER_TWO_ROOT_URL}/wines.xml"
  PROD_SUPPLIER_TWO_URL = "#{PROD_SUPPLIER_TWO_ROOT_URL}/wines.xml"
  
  #Orders
  DEV_SUPPLIER_ONE_ORDER_URL = "#{DEV_SUPPLIER_ONE_ROOT_URL}/orders"
  DEV_SUPPLIER_TWO_ORDER_URL = "#{DEV_SUPPLIER_TWO_ROOT_URL}/orders"
  PROD_SUPPLIER_ONE_ORDER_URL = "#{PROD_SUPPLIER_ONE_ROOT_URL}/orders"
  PROD_SUPPLIER_TWO_ORDER_URL = "#{PROD_SUPPLIER_TWO_ROOT_URL}/orders"

  def get_session_id
  	session[:customer_id]
  end

  def get_customer
  	customer = Customer.find(session[:customer_id])
  	c = Customer.new
  	c.first_name = customer.first_name
  	c.surname = customer.surname
  	c.email = customer.email
  	c
  end
  
  protected

  # Checks if a user has a shopping cart
  def has_cart?
    if !current_cart.nil?
      true
    end
  end

  private
    def current_cart
      Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      cart = Cart.create
      session[:cart_id] = cart.id
      cart
    end

    # Gets wine's absolute URL as stored on the server and builts a direct URL accordingly    
    def get_wine_image_url(wine)
      wine_relative_url = wine.image_url
      wine_relative_url = wine_relative_url[23..wine_relative_url.length]
      
      if wine.supplier == SUPPLIER_ONE_NAME
        if Rails.env.development?
          @image_url =  "#{DEV_SUPPLIER_ONE_ROOT_URL}#{wine.image_url}"
        elsif Rails.env.production?
          @image_url =  "https://s3-eu-west-1.amazonaws.com#{wine_relative_url}"
        end
      elsif wine.supplier == SUPPLIER_TWO_NAME
        if Rails.env.development?
          @image_url =  "#{DEV_SUPPLIER_TWO_ROOT_URL}#{wine.image_url}"
        elsif Rails.env.production?
          @image_url =  "https://s3-eu-west-1.amazonaws.com#{wine_relative_url}"
        end
      end
      @image_url
    end
end
