require 'rest_client'
require 'rubygems'

class CartsController < ApplicationController
  include CartsHelper
  before_action :set_cart, only: [:show, :edit, :update]
  helper_method :post_to_supplier, :get_wine_image_url 

  respond_to :html

  def index
    respond_to do |format|
      format.html { redirect_to wines_url}
      format.json { render json: @customers }
    end 

    @carts = Cart.all
  end

  def show
    begin
      @cart = Cart.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to wines_url(page: @current_page), notice: 'Invalid cart'
    rescue Exception
      redirect_to wines_url(page: @current_page), notice: 'Invalid cart'
    else
      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @cart }
      end
    end
  end 

  def create
    # First check if customer is logged in before processing checkout
    # If not logged in, redirect them to login page
    if !session[:customer_id].nil?
      @cart = current_cart
      @supplier_one_order_url, @supplier_two_order_url = get_order_url

      # Process orders and send to the appropriate suppliers
      if post_to_supplier(@cart, SUPPLIER_ONE_NAME) == true
        @cart.line_items.each do |item|
          if item.wine.supplier == SUPPLIER_ONE_NAME     
            RestClient.post "#{@supplier_one_order_url}", build_order_detail_json(item), :content_type => :json, :accept => :json
          end
        end
      end

      if post_to_supplier(@cart, SUPPLIER_TWO_NAME) == true
        @cart.line_items.each do |item|
          if item.wine.supplier == SUPPLIER_TWO_NAME
            RestClient.post "#{@supplier_two_order_url}", build_order_detail_json(item), :content_type => :json, :accept => :json
          end
        end
      end    

      #clean up cart after checkout
      @cart.destroy
      session[:cart_id] = nil
      
      respond_to do |format|
        format.html { redirect_to wines_url(page: @current_page),
        notice: 'Your Order is successful and your shopping cart is empty' }
        format.json { head :no_content }
      end
    else
      session[:to_checkout] = true
      respond_to do |format|
        format.html { redirect_to root_path,
        notice: 'Please login to checkout successfully' }
        format.json { head :no_content }
      end
    end
  end

  def get_order_url
    @suppler_one_order_url = DEV_SUPPLIER_ONE_ORDER_URL
    @suppler_two_order_url = DEV_SUPPLIER_TWO_ORDER_URL
    
    if Rails.env.production?
      @suppler_one_order_url = PROD_SUPPLIER_ONE_ORDER_URL
      @suppler_two_order_url = PROD_SUPPLIER_TWO_ORDER_URL
    end   

    return @suppler_one_order_url, @suppler_two_order_url
  end


  def new
    @cart = Cart.new
    respond_with(@cart)
  end

  def edit
  end


  def update
    @cart.update(cart_params)
    respond_with(@cart)
  end

  def destroy
    @cart = current_cart
    @cart.destroy
    session[:cart_id] = nil
    
    respond_to do |format|
      format.html { redirect_to wines_url(page: @current_page),
      notice: 'Your cart is currently empty' }
      format.json { head :no_content }
    end
  end

  private
    # Set carts, but also importantly checks that invalid cart IDs typed into URL
    # are redirected to wines home page with the right notice
    def set_cart
      begin
        @cart = Cart.find(params[:id])
        if @cart.line_items.length == 0       
          redirect_to wines_url(page: @current_page), notice: 'Shopping Cart is empty'
        end
      rescue ActiveRecord::RecordNotFound
        redirect_to wines_url(page: @current_page), notice: 'Invalid cart'
      else
        #respond_to do |format|
         # format.html # show.html.erb
         # format.json { render json: @cart }
        #end
      end
    end

    def cart_params
      params[:cart]
    end
end
