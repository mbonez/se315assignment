class LineItemsController < ApplicationController
  before_action :set_line_item, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    respond_to do |format|
      format.html { redirect_to wines_url}
      format.json { render json: @customers }
    end   
    @line_items = LineItem.all
  end

  def show
    @cart = current_cart
    respond_with(@line_item)
  end

  def new
    @cart = current_cart
    @line_item = LineItem.new
    respond_with(@line_item)
  end

  def edit
     @cart = current_cart
  end

  # Creates new lineItem in a cart
  def create
    @cart = current_cart
    wine = Wine.find(params[:wine_id])

    # Gets Quantity and convert to integer
    quantity = params[:the_quantity].to_i

    # Add new wine and quantity using the add_wine method from Cart.rb
    # Sets quantity to a default of '1' if quantity isn't specified
    @line_item = @cart.add_wine(wine.id, quantity > 0 ? quantity : 1)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to @line_item.cart}
        format.json { render json: @line_item, status: :created, location: @line_item }
      else
        format.html { render action: "new" }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @line_item.update(line_item_params)
    respond_with(@line_item)
  end

  def destroy
    @cart = current_cart
    @line_item.destroy
    respond_to do |format|
      if @cart.line_items.length > 0
        format.html { redirect_to @line_item.cart}
        format.json { render json: @line_item, status: :created, location: @line_item }      
      else
        format.html { redirect_to wines_url(page: @current_page),
        notice: 'Your cart is currently empty' }
        format.json { head :no_content }
      end
    end
    #respond_with(@line_item)
  end

  private
    def set_line_item
      @line_item = LineItem.find(params[:id])
    end

    def line_item_params
      params.require(:line_item).permit(:wine_id, :cart_id, :the_quantity)
    end
end
