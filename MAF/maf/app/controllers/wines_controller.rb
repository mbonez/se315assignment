require 'rexml/document'
require 'open-uri'

class WinesController < ApplicationController
  include ApplicationHelper
  before_action :set_current_page, except: [:index]
  before_action :set_wine, only: [:show, :edit, :update, :destroy]
  helper_method :get_wine_image_url, :get_supplier_data_state


  # GET /wines
  # GET /wines.json
  def index
    @cart = current_cart
    get_all_wines(false)
    @wines = Wine.paginate(:page => params[:page],:per_page => params[:per_page]).order('name')
  end

  # GET /wines/1
  # GET /wines/1.json
  def show
    @cart = current_cart
  end

  # GET /wines/new
  def new
    @cart = current_cart
    @wine = Wine.new
  end

  # GET /wines/1/edit
  def edit
    @cart = current_cart
  end

  # Search method is called to search from cached wine data in the database
  def search
    @cart = current_cart
    @wines = Wine.like(params[:query]).paginate(page: params[:page], per_page: params[:per_page]).order('name')
    if request.xhr?
      render partial: 'wines_list', object: @wines, layout: false
    else
      render 'index'
    end
  end

  def price_filter
    @cart = current_cart
    if params[:lowest_price_query].nil? || params[:lowest_price_query] == 0
      params[:lowest_price_query] = 0
    end
    if params[:highest_price_query].nil? || params[:highest_price_query] < params[:lowest_price_query] || params[:highest_price_query] == 0
      params[:highest_price_query] = 999
    end

    if params[:lowest_price_query] == "" && params[:highest_price_query] == ""
      params[:lowest_price_query] = 0
      params[:highest_price_query] = 999
    end

    @wines = Wine.price_like(params[:lowest_price_query], params[:highest_price_query]).paginate(page: params[:page], per_page: params[:wine_per_page]).order('name')
    if request.xhr?
      render partial: 'wines_list', object: @wines, layout: false
    else
      render 'index'
    end
  end

  def get
  end

  # POST /wines
  # POST /wines.json
  def create
    respond_to do |format|
      if @wine.save
        format.html { redirect_to @wine, notice: 'Wine was successfully created.' }
        format.json { render :show, status: :created, location: @wine }
      else
        format.html { render :new }
        format.json { render json: @wine.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /wines/1
  # PATCH/PUT /wines/1.json
  def update
    respond_to do |format|
      if @wine.update(wine_params)
        format.html { redirect_to @wine, notice: 'Wine was successfully updated.' }
        format.json { render :show, status: :ok, location: @wine }
      else
        format.html { render :edit }
        format.json { render json: @wine.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wines/1
  # DELETE /wines/1.json
  def destroy
    @wine.destroy
    respond_to do |format|
      format.html { redirect_to wines_url(page: @current_page), notice: 'Wine was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_wine
    @wine = Wine.find(params[:id])
  end

  def set_current_page
    @current_page= params[:page] || 1
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def wine_params
    params.require(:wine).permit(:id, :lowest_price_query, :query, :highest_price_query, :wine_per_page , :wine_unique_number , :name, :image_url, :short_description,
                                 :bottle_size_in_ml, :price, :long_description, :country_of_origin, :grade_type, :suitable_for_vegetarians, :supplier, :image_file_name, :last_modified)
  end
end
