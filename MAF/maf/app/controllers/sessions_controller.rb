class SessionsController < ApplicationController
  def new
    @cart = current_cart
  end

  # Create new user session and set where a user is redirected to
  # based on if they are logged in or not.
  # Also redirects to checkout page if need be depending on the result of the switch session[:to_checkout].
  def create
  	customer = Customer.find_by_email(params[:email])
  	if customer and customer.authenticate(params[:password])
  		session[:customer_id] = customer.id
      if session[:to_checkout] == false
  		  redirect_to wines_path
      else
        redirect_to "/carts/#{session[:cart_id]}"
      end
  	else
  		redirect_to root_path, alert: "Invalid user/password combination"
  	end    
  end

  # Delete session data once the user clicks log out
  def destroy
    session[:to_checkout] = false
  	session[:customer_id] = nil
    session[SUPPLIER_ONE_NAME] = nil
    session[SUPPLIER_TWO_NAME] = nil
    session[:cart_id] = nil
    Cart.delete_all
    Cart.destroy_all
    LineItem.destroy_all
    LineItem.delete_all
	  redirect_to root_path, notice: "Logged out"
  end
end
