/* Create a new object prototype with a constructor function */
function PriceFilter(form) {
  this.form = form;
  
  this.queue = function(lowest_price_query, highest_price_query, wine_per_page){
    if (this.timer){
      clearTimeout(this.timer);
    }
    var self = this;
    this.timer = setTimeout(function () {
      self.priceFilter(lowest_price_query, highest_price_query, wine_per_page);
    }, 150);
  };

  this.priceFilter = function(lowest_price_query, highest_price_query, wine_per_page){
    var self = this;
    jQuery.ajax({
      url: this.form.action,
      type: this.form.method,
      data: {'lowest_price_query': lowest_price_query, 'highest_price_query': highest_price_query, 'wine_per_page': wine_per_page},
      success: function(results) {self.render(results)},
      error: function (request, status, error) {
        alert(request.responseText);
      },
      dataType: 'html'
    });
  };

  this.render = function(results){  
    $('#main-input-area').html(results);
  };
}

/* 
jQuery.fn is an alias for jQuery.prototype and allows
us to dynamically extend the jQuery prototype. We're defining
a new method on jQuery called search 
*/
jQuery.fn.priceFilter = function() {
  /* 
  We iterate over each of the jQuery objects associated with the
  current jQuery object (actually in this case will be just one) 
  */ 
  this.each(function () {
    /* Create an object using the prototype constructor */
    var priceFilter = new PriceFilter(this);
    var lowestPrice = 0;
    var highestPrice = 999;
    var wine_per_page = $('#wine_per_page :selected').text();

    $("#price_filter input[type=text]:eq(0)").on('keyup', function () {
      lowestPrice = this.value
      priceFilter.queue(lowestPrice, highestPrice, wine_per_page);
    });

    $("#price_filter input[type=text]:eq(1)").on('keyup', function () {
      highestPrice = this.value
      priceFilter.queue(lowestPrice, highestPrice, wine_per_page);
    });

    $('#wine_per_page').on('change', function () {
      wine_per_page = $('#wine_per_page :selected').text();
      $('#wine_per_page').val(wine_per_page)
      priceFilter.queue(lowestPrice, highestPrice, wine_per_page);
    });

  });
};

/* 
The "ready" event is fired when the DOM is built.
However, to deal with turbolinks we also need to bind
to the "page:change" event. This is because Turbolinks
uses Ajax to just update the body part
of a DOM tree when you click a link. So without "page:change" 
or "page:load" your JS functions would not be rebound to the updated
part of the DOM. The "page:change" event will be fired by Turbolinks when the 
body element of the DOM is changed 
*/
jQuery(function ($) {
  $(document).on('ready page:load', function(){ 
    $('#price_filter').priceFilter();
  });
});