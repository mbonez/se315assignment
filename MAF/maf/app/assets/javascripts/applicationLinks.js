var ready;
ready = function() {
  $('.next_page').click(function(){ 
    $('.next_page').css('cursor','wait');
  });

  $('.previous_page').click(function(){ 
    $('.previous_page').css('cursor','wait');
  });

  $('.selected').click(function(){ 
    $('.selected').css('cursor','wait');
  });  

  $('.navigation #wines').click(function(){ 
    $('.navigation #wines').css('cursor','wait');
  });
};

$(document).ready(ready);
$(document).on('page:load', ready);