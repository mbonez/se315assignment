Feature: Login
	As a customer
	I want to be able to log into the website using my email and password, or continue without signing in
	So that I can access wines lists

	Background:
		Given we have the following Customers:
		 	| first_name | surname  | address      | email         | password |
		 	| Olu		 | Ashiru	| TestAddress1 | olu@gmail.com | olupass  |
		 	| Ade		 | Ashiru	| TestAddress2 | ade@gmail.com | adepass  |
		 	| Omo		 | Ashiru	| TestAddress3 | omo@gmail.com | omopass  |

	Scenario: Login successfully using Email and Password
	 	Given I am on the welcome page
	 	And I fill in "email" with "olu@gmail.com"
	 	And I fill in "password" with "olupass"
	 	When I press button "Login"
	 	Then I should be on the wines page
	 	And I should be welcomed as "Olu Ashiru"

 	Scenario: Login Unsuccessfully using Email and wrong Password
	 	Given I am on the welcome page
	 	And I fill in "email" with "olu@gmail.com"
	 	And I fill in "password" with "olupass_wrong"
	 	When I press button "Login"
	 	Then page should have notice message "Invalid user/password combination"
	 	And I should remain on the welcome page

 	Scenario: Login Unsuccessfully using wrong Email and right Password
	 	Given I am on the welcome page
	 	And I fill in "email" with "wrong@gmail.com"
	 	And I fill in "password" with "olupass"
	 	When I press button "Login"
	 	Then page should have notice message "Invalid user/password combination"
	 	And I should remain on the welcome page

	Scenario: Continue to see the wines list without logging in
	 	Given I am on the welcome page
	 	When I press buttton "Continue without signing in"
	 	Then I should be on the wines page
	 	And I should be welcomed as Guest

	Scenario: Login and Logout successfully
		Given I am on the welcome page
	 	And I fill in "email" with "olu@gmail.com"
	 	And I fill in "password" with "olupass"
	 	And I press button "Login"
	 	And I am on the wines page
	 	When I press button "Logout"
	 	Then I should be on the welcome page