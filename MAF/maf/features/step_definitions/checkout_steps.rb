Then(/^And I should see "(.*?)"$/) do |info|
	page.should have_content(info)
end

Given(/^I should not see "(.*?)"$/) do |info|
	page.should have_no_content(info)
end