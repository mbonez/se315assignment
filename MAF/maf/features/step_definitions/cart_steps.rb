Given(/^I fill in "(.*?)" of a wine with "(.*?)"$/) do |quantity_id, quantity|
	wines = Wine.all
	visit("/wines")
	find(:xpath, '/html/body/div/div[2]/div[2]/div/div[1]/div[3]/div/div/form/p/input[1]').set("#{quantity}")
	#fill_in "#{quantity_id}_#{wines[3].id}", :with => "#{quantity}"
end

When(/^I press button "(.*?)" for a wine$/) do |button_name|
	wines = Wine.all
	visit("/wines")	
	find(:xpath, '/html/body/div/div[2]/div[2]/div/div[1]/div[3]/div/div/form/p/input[2]').click()
	#click_button("addToBasket_#{wines[3].id}")
end

Then(/^I should see "(.*?)"$/) do |info|
  page.should have_content(info)
end

Then(/^I should see wine name$/) do
	wines = Wine.all	
	page.should have_content(wines[2].name)
end

Given(/^I fill in "(.*?)" of the wine with "(.*?)"$/) do |quantity_id, quantity|
	wines = Wine.all
	fill_in "the_quantity", :with => "#{quantity}"
end

When(/^I press button "(.*?)" for the wine$/) do |button_name|
	wines = Wine.all
	click_button("addToBasket")
end

When(/^I press button "(.*?)" for the second wine$/) do |button_name|
	wines = Wine.all
	find(:xpath, '/html/body/div/div[2]/div[2]/div/div[1]/div[4]/div/div/form/p/input[2]').click()
end

Given(/^I see "(.*?)"$/) do |info|
	page.should have_content(info)
end

Then(/^I should not see wine name$/) do
	wines = Wine.all	
	page.should have_no_content(wines[3].name)
end

Given(/^I press the second wine on the list$/) do
	wines = Wine.all	
	visit("/wines/#{wines[3].id}")
end
