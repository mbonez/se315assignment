Given(/^I am on the (.+) page$/) do |page_name|
	visit("/")
end

Given(/^I fill in "(.*?)" with "(.*?)"$/) do |field, value|
	fill_in(field, with: value)
end

Given(/^we have the following Customers:$/) do |customers|
	customers_attributes = customers.hashes.map { |tab_customer_attrs|
		customer_attrs = FactoryGirl.attributes_for(:customer).stringify_keys
		customer_attrs.merge(tab_customer_attrs)
	}
	Customer.create!(customers_attributes)
end

When(/^I press button "(.*?)"$/) do |button_name|
	click_button(button_name)
end

Then(/^I should be on the (.+) page$/) do |page_name|
	if page_name == "welcome"
		visit("/")
	else
		visit("/#{page_name}")
	end
end

Then(/^page should have notice message "(.*?)"$/) do |message|
	page.should have_content(message)
end

Then(/^I should remain on the (.+) page$/) do |page_name|
	visit("/")
end

Then(/^I should be welcomed as a (.+)$/) do |user_name|
	page.should have_content(user_name)
end

Then(/^I should be welcomed as "(.*?)"$/) do |user_name|
	page.should have_content(user_name)
end

Then(/^I should be welcomed as Guest$/) do
	page.should have_content("Guest")
end
