Given(/^we have the following Wines:$/) do |wines|
	wines_attributes = wines.hashes.map { |tab_wine_attrs|
		wine_attrs = FactoryGirl.attributes_for(:wine).stringify_keys
		wine_attrs.merge(tab_wine_attrs)
	}
	Wine.create!(wines_attributes)
	visit("/wines")
end

Then(/^I should be on page (\d+) of (.+)$/) do |page_number, page_name|
	visit("/#{page_name}?page=#{page_number}")
end

Given(/^I am on page (\d+) of (.+)$/) do |page_number, page_name|
	visit("/#{page_name}?page=#{page_number}")
end


When(/^I press link "(.*?)"$/) do |link_name|
	click_link(link_name)
end

When(/^I press buttton "(.*?)"$/) do |button_name|
	click_button(button_name)
end

When(/^I press the first wine on the list$/) do
	wines = Wine.all	
	visit("/wines/#{wines[2].id}")		
end

Then(/^I should see "(.*?)" on the page$/) do |text|
	page.should have_content(text)
end
