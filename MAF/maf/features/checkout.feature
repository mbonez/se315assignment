Feature: Checkout
	As a customer
	I want to be able to checkout based on what's in my cart
	So that I can have my items precessed successfully

	Background:
		Given we have the following Wines:
		 	| name       | short_description  | price | supplier          | bottle_size_in_ml | image_url 																						 |
		 	| TestWineA  | TestWineAShortDec  | 10   | Welsh Wine Shop   | 1                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineB  | TestWineBShortDec  | 20   | English Wine Shop | 2                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineC  | TestWineCShortDec  | 30   | Welsh Wine Shop   | 3                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
	 	And we have the following Customers:
		 	| first_name | surname  | address      | email         | password |
		 	| Olu		 | Ashiru	| TestAddress1 | olu@gmail.com | olupass  |
		 	| Ade		 | Ashiru	| TestAddress2 | ade@gmail.com | adepass  |
		 	| Omo		 | Ashiru	| TestAddress3 | omo@gmail.com | omopass  |

	Scenario: Attempt to checkout after adding wines without login
		Given I am on the wines page
	 	And I press the first wine on the list
	 	And I fill in "the_quantity" of the wine with "2"	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I press link "Continue Shopping"
	 	And I press the second wine on the list	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I press button "Checkout"
	 	Then I should be on the welcome page

	Scenario: Checkout successfully after adding wines and logging in
		Given I am on the wines page
	 	And I press the first wine on the list
	 	And I fill in "the_quantity" of the wine with "2"	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I press link "Continue Shopping"
	 	And I press the second wine on the list	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I press button "Checkout"
	 	And I am on the welcome page
	 	And I fill in "email" with "olu@gmail.com"
	 	And I fill in "password" with "olupass"
	 	And I press button "Login"
	 	And I should not see "Guest"
	 	And I press button "Checkout"
	 	Then I should see "Your Order is successful and your shopping cart is empty"
	 	And I should see "Shopping Cart is Empty"