Feature: Cart
	As a customer
	I want to be able to add and remove item in cart.
	So that I can have a shopping basket

	Background:
		Given we have the following Wines:
		 	| name       | short_description  | price | supplier          | bottle_size_in_ml | image_url 																						 |
		 	| TestWineA  | TestWineAShortDec  | 10   | Welsh Wine Shop   | 1                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineB  | TestWineBShortDec  | 20   | English Wine Shop | 2                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineC  | TestWineCShortDec  | 30   | Welsh Wine Shop   | 3                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |

	Scenario: Add a wine to shopping basket from the wines list page
	 	Given I am on the wines page
	 	And I fill in "the_quantity" of a wine with "1"
	 	When I press button "Add to basket" for a wine
	 	Then I should see "Your Cart"
	 	And I should see wine name

	Scenario: Add a wine to shopping basket from a lone wine detail page
	 	Given I am on the wines page
	 	And I press the first wine on the list
	 	And I fill in "the_quantity" of the wine with "1"	 	
	 	When I press button "Add to basket" for the wine
	 	Then I should see "Your Cart"
	 	And I should see wine name 	

	Scenario: Remove all wine instance from cart using the Remove Items button
	 	Given I am on the wines page
	 	And I press the first wine on the list
	 	And I fill in "the_quantity" of the wine with "3"	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I should see wine name
	 	When I press button "Remove Item(s)"
	 	Then I should be on the wines page
	 	And I should see "Shopping Cart is Empty"

	Scenario: Navigate from cart back to wines
	 	Given I am on the wines page
	 	And I press the first wine on the list
	 	And I fill in "the_quantity" of the wine with "1"	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	When I press link "Continue Shopping"
	 	Then I should be on the wines page

		
	Scenario: Navigate to cart, back to wines, and back to cart
	 	Given I am on the wines page
	 	And I press the first wine on the list
	 	And I fill in "the_quantity" of the wine with "1"	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I press link "Continue Shopping"
	 	And I press link "View Shopping Cart"
	 	Then I should see "Your Cart"

 	Scenario: Empty Cart
	 	Given I am on the wines page
	 	And I press the first wine on the list
	 	And I fill in "the_quantity" of the wine with "1"	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I press link "Continue Shopping"
	 	And I press the second wine on the list	 	
	 	And I press button "Add to basket" for the wine
	 	And I see "Your Cart"
	 	And I press button "Empty cart"
	 	Then I should be on the wines page
	 	And I should see "Shopping Cart is Empty"