require 'factory_girl'

FactoryGirl.define do
	factory :customer do |f|
		f.first_name	'testolu_firstname'
		f.surname		'testAshiru_surname'
		f.email			'testolu@gmail.com'
		f.password		'testpassword'
	end	

	factory :wine do |f|
		f.name					'test_winename'
		f.short_description		'test_short_desc'
		f.price					0.0
		f.supplier				'test_supplier'
		f.bottle_size_in_ml		'test_bottle_size'
		f.image_url				'test_image_url'
	end
end