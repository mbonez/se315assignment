Feature: WinesNavigate
	As a customer
	I want to be able to navigate through the list of wines on the website
	So that I can see details of wines

	Background:
		Given we have the following Wines:
		 	| name       | short_description  | price | supplier          | bottle_size_in_ml | image_url 																						 |
		 	| TestWineA  | TestWineAShortDec  | 10   | Welsh Wine Shop   | 1                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineB  | TestWineBShortDec  | 20   | English Wine Shop | 2                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineC  | TestWineCShortDec  | 30   | Welsh Wine Shop   | 3                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineD  | TestWineDShortDec  | 40   | Welsh Wine Shop   | 4                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineE  | TestWineEShortDec  | 50   | English Wine Shop | 5                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineF  | TestWineFShortDec  | 60   | Welsh Wine Shop   | 6                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineG  | TestWineGShortDec  | 70   | Welsh Wine Shop   | 7                 | http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |
		 	| TestWineH  | TestWineHShortDec  | 80   | English Wine Shop | 8                 |	http://localhost:3001/system/wines/images/000/000/100/original/wine_PNG9479.png?1447209069 |

	Scenario: Navigate through wines from First to Second page
		Given I am on page 1 of wines
		When I press link "Next"
		Then I should be on page 2 of wines

	Scenario: Navigate through wines from Second page to First
		Given I am on page 2 of wines
		When I press link "Previous"
		Then I should be on page 1 of wines

	Scenario: Navigate from wines list to view wine details
		Given I am on page 1 of wines
		When I press the first wine on the list
		Then I should see "Suitable For Vegetarians" on the page